# [antikvar.info](https://antikvar.info) source codes

<br/>

### Run antikvar.info on localhost

    # vi /etc/systemd/system/antikvar.info.service

Insert code from antikvar.info.service

    # systemctl enable antikvar.info.service
    # systemctl start antikvar.info.service
    # systemctl status antikvar.info.service

http://localhost:4038
